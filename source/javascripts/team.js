$('#team-selector').on('change', function (e) {
  if (this.value == 'All') {
    $('.member').removeClass('hidden');
  }
  else {
    $('.member').addClass('hidden');
    $('.member[data-departments*="' + this.value + '"]').removeClass('hidden');
  }
});
