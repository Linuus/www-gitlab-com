---
layout: job_page
title: "Billing Specialist"
---

We’re looking for a billing specialist that can manage the invoicing and collection process in its entirety. Specifically, we’re looking for someone to orchestrate and own the customer billing cycle from quote to cash. This role will collaborate with the sales team and at times with customers.  Attention to detail and an aptitude for accuracy are critical to being successful.

We are a rapidly growing company which means you must be comfortable working in fast paced atmosphere where juggling numerous priorities and deadlines is the norm. We are also a fully distributed team which means you must be self-driven and a highly effective communicator.

## Responsibilities

- Completely own and manage the billing process
- Work closely with the Sales team on various billing related issues, including quote generation
- Assist in cross-functional accounting activities as needed
- Resolving customer inquiries in an accurate, timely manner
- Identify process and system improvements to streamline the revenue cycle
- Responsible for owning and administering sales commissions using Xactly
- Communicate process improvements by routinely and frequently updating our handbook and training the sales team.
- Financial reporting and analytics of sales figures and A/R data
- Proactively monitor aging reports, following up on delinquent accounts and managing A/R collections
- Provide sales and billing reports to upper management as needed
- Ability to take on side projects related to internal initiatives

## Requirements

- Proven ability to fully utilize the Zuora platform from quote to cash collection
- Experience with Salesforce CRM
- Experience billing in a high-volume environment
- Deep understanding of subscription billing
- Superior attention to detail
- Excellent computer skills, self starter in picking up new and complex systems
- Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets,etc).
- Slack is a plus but not necessary
- Bonus points: experience with Xactly
- Bonus points: experience using NetSuite
- Bonus points: experience working with distributed teams
